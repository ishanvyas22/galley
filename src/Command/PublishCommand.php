<?php
declare(strict_types=1);

namespace Galley\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

class PublishCommand extends Command
{
    /**
     * Get the command name.
     *
     * Returns the command name based on class name.
     * For e.g. for a command with class name `UpdateTableCommand` the default
     * name returned would be `'update_table'`.
     *
     * @return string
     */
    public static function defaultName(): string
    {
        return 'galley publish';
    }

    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/4/en/console-commands/commands.html#defining-arguments-and-options
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser = parent::buildOptionParser($parser);

        $parser->setDescription('Publish Galley\'s Docker files to project directory.');

        return $parser;
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|void|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        system(sprintf('cp -r "%s" docker', __DIR__ . '/../../runtimes/'));

        file_put_contents(
            'docker-compose.yml',
            str_replace(
                [
                    './vendor/amayer5125/galley/runtimes/7.4',
                    './vendor/amayer5125/galley/runtimes/8.0',
                    './vendor/amayer5125/galley/runtimes/8.1',
                ],
                [
                    './docker/7.4',
                    './docker/8.0',
                    './docker/8.1',
                ],
                file_get_contents('docker-compose.yml')
            )
        );
    }
}
