<?php
declare(strict_types=1);

namespace Galley\Command;

use Cake\Collection\Collection;
use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Datasource\ConnectionManager;

class InstallCommand extends Command
{
    /**
     * Get the command name.
     *
     * Returns the command name based on class name.
     * For e.g. for a command with class name `UpdateTableCommand` the default
     * name returned would be `'update_table'`.
     *
     * @return string
     */
    public static function defaultName(): string
    {
        return 'galley install';
    }

    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/4/en/console-commands/commands.html#defining-arguments-and-options
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser = parent::buildOptionParser($parser);

        $parser->setDescription('Install Galley\'s default Docker Compose file.')
            ->addOption('with', [
                'help' => 'The services that should be included in the installation',
                'default' => 'mysql,redis',
            ]);

        return $parser;
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|void|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $services = (new Collection(explode(',', $args->getOption('with'))))
            ->filter(function ($service) {
                return in_array($service, ['mysql', 'redis']);
            });

        $dockerCompose = $this->buildDockerCompose($services, $io);
        $io->createFile('docker-compose.yml', $dockerCompose);
        $io->out('');

        if ($services->contains('mysql')) {
            $environment = $this->replaceEnvVariables($services);
            $io->createFile('config/.env', $environment);
            $io->out('');

            $io->out('Please replace "Datasources.default.host" configuration with the hostname "mysql".');
            $io->out('You may also wish to copy the mysql docker service for the testing database.', 2);
        }

        if ($services->contains('redis')) {
            $io->out('Please replace "Cache.x.className" configuration with "\Cake\Cache\Engine\RedisEngine".', 2);
        }

        $io->info('Galley scaffolding installed successfully.');
    }

    /**
     * Build the Docker Compose file.
     *
     * @param \Cake\Collection\Collection $services
     * @return string
     */
    protected function buildDockerCompose(Collection $services): string
    {
        $depends = $services
            ->map(function ($service) {
                return "      - {$service}";
            });
        if (! $depends->isEmpty()) {
            $depends = "depends_on:\n" . implode("\n", $depends->toArray());
        }

        $stubs = $services
            ->map(function ($service) {
                return file_get_contents(__DIR__ . "/../../stubs/{$service}.stub");
            });
        if (! $stubs->isEmpty()) {
            $stubs = implode('', $stubs->toArray());
        }

        $volumes = $services
            ->map(function ($service) {
                return "  galley-{$service}:\n    driver: local";
            });
        if (! $volumes->isEmpty()) {
            $volumes = "volumes:\n" . implode("\n", $volumes->toArray());
        }

        $dockerCompose = file_get_contents(__DIR__ . '/../../stubs/docker-compose.stub');

        $dockerCompose = str_replace('{{depends}}', empty($depends) ? '' : '    ' . $depends, $dockerCompose);
        $dockerCompose = str_replace('{{services}}', $stubs, $dockerCompose);
        $dockerCompose = str_replace('{{volumes}}', $volumes, $dockerCompose);

        // Remove empty lines...
        $dockerCompose = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $dockerCompose);

        return $dockerCompose;
    }

    /**
     * Replace the Host environment variables in the app's .env file.
     *
     * @param \Cake\Collection\Collection $services
     * @return string
     */
    protected function replaceEnvVariables(Collection $services): string
    {
        $envFiles = [
            'config/.env',
            'config/.env.example',
            __DIR__ . '/../../stubs/env.stub',
        ];

        foreach ($envFiles as $envFile) {
            if (file_exists($envFile)) {
                $environment = file_get_contents($envFile);
                break;
            }
        }

        $databaseConfig = ConnectionManager::getConfig('default');

        if (preg_match('/DB_DATABASE="(.*)"/', $environment)) {
            $environment = preg_replace('/DB_DATABASE="(.*)"/', "DB_DATABASE=\"{$databaseConfig['database']}\"", $environment);
        } else {
            $environment .= "export DB_DATABASE=\"{$databaseConfig['database']}\"\n";
        }

        if (preg_match('/DB_USERNAME="(.*)"/', $environment)) {
            $environment = preg_replace('/DB_USERNAME="(.*)"/', "DB_USERNAME=\"{$databaseConfig['username']}\"", $environment);
        } else {
            $environment .= "export DB_USERNAME=\"{$databaseConfig['username']}\"\n";
        }

        if (preg_match('/DB_PASSWORD="(.*)"/', $environment)) {
            $environment = preg_replace('/DB_PASSWORD="(.*)"/', "DB_PASSWORD=\"{$databaseConfig['password']}\"", $environment);
        } else {
            $environment .= "export DB_PASSWORD=\"{$databaseConfig['password']}\"\n";
        }

        return $environment;
    }
}
